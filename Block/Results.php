<?php

namespace Survey\SurveyPage\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Survey\SurveyPage\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;
use Magento\Catalog\Model\ProductFactory as CatalogProductFactory;

class Results extends Template
{
    /**
     * @var AnswerCollectionFactory
     */
    private $answerCollectionFactory;
    /**
     * @var CatalogProductFactory
     */
    private $catalogProductFactory;

    /**
     * Results constructor.
     * @param Context $context
     * @param AnswerCollectionFactory $answerCollectionFactory
     * @param CatalogProductFactory $catalogProductFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        AnswerCollectionFactory $answerCollectionFactory,
        CatalogProductFactory $catalogProductFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->answerCollectionFactory = $answerCollectionFactory;
        $this->catalogProductFactory = $catalogProductFactory;
    }

    public function getAnswersData(){
        $result = [];

        $collection = $this->answerCollectionFactory->create();
        $collection->getSelect()
            ->columns([
                'avg' => 'AVG(rating)'
            ])
            ->group('product_id');

        if($collection->count()){
            foreach($collection->getData() as $item){
                $result[] = [
                    'product' => $this->catalogProductFactory->create()->load($item['product_id'])->getName(),
                    'avg' => $item['avg']
                ];
            }
        }

        return json_encode($result);
    }
}