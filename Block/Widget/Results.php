<?php

namespace Survey\SurveyPage\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\FilterBuilder;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Survey\SurveyPage\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;

class Results extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SearchCriteriaInterface
     */
    private $searchCriteria;
    /**
     * @var FilterGroup
     */
    private $filterGroup;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var Status
     */
    private $productStatus;
    /**
     * @var Visibility
     */
    private $productVisibility;
    /**
     * @var AnswerCollectionFactory
     */
    private $answerCollectionFactory;

    /**
     * Results constructor.
     * @param Template\Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaInterface $searchCriteria
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param AnswerCollectionFactory $answerCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaInterface $searchCriteria,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility,
        AnswerCollectionFactory $answerCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->setTemplate('results_widget.phtml');
        $this->productRepository = $productRepository;
        $this->searchCriteria = $searchCriteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->answerCollectionFactory = $answerCollectionFactory;
    }

    public function getProductAverageRating()
    {
        if(!empty($this->getData('product'))){
            $product = $this->getProducts($this->getData('product'));
            $response = 'Average rating for product "' . $product->getName() . '" is ' . $this->getAvg([$product->getId()]);
        }else{
            $products = $this->getProducts();
            $productIds = [];

            if(!empty((array)$products) && is_array((array)$products)){
                foreach($products as $product){
                    $productIds[] = $product->getId();
                }
            }

            $response = 'Average rating for all products is ' . $this->getAvg($productIds);
        }

        return $response;
    }

    public function getProducts($id = null){
        if(!is_null($id)){
            $productItems = $this->productRepository->getById($id);
        }else{
            $filters = [
                $this->filterBuilder
                    ->setField('status')
                    ->setConditionType('in')
                    ->setValue($this->productStatus->getVisibleStatusIds())
                    ->create(),
                $this->filterBuilder
                    ->setField('visibility')
                    ->setConditionType('in')
                    ->setValue($this->productVisibility->getVisibleInSiteIds())
                    ->create()
            ];

            $this->filterGroup->setFilters($filters);

            $this->searchCriteria->setFilterGroups([$this->filterGroup]);
            $products = $this->productRepository->getList($this->searchCriteria);
            $productItems = $products->getItems();
        }

        return $productItems;
    }

    public function getAvg($productIds)
    {
        $result = 0;

        if(!empty($productIds) && is_array($productIds)){
            $answers = $this->answerCollectionFactory->create()
                ->addFieldToFilter('product_id', ['in' => $productIds])
                ->getItems();

            if(!empty($answers) && is_array($answers)) {
                foreach ($answers as $answer){
                    $result += $answer->getRating();
                }

                $result = $result / count($answers);
            }
        }

        return $result;
    }
}