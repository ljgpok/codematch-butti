<?php

namespace Survey\SurveyPage\Model\Config\Source;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\FilterBuilder;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;

class Select implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var SearchCriteriaInterface
     */
    private $searchCriteria;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var FilterGroup
     */
    private $filterGroup;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var Status
     */
    private $productStatus;
    /**
     * @var Visibility
     */
    private $productVisibility;

    /**
     * Select constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaInterface $searchCriteria
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaInterface $searchCriteria,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility
    )
    {
        $this->searchCriteria = $searchCriteria;
        $this->productRepository = $productRepository;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
    }

    public function toOptionArray()
    {
        $productOptions = [
            [
                'value' => '',
                'label' => '--Please select--'
            ]
        ];

        $products = $this->getProducts();

        if(!empty((array) $products) && is_array((array)$products)){
            foreach($products as $product){
                $productOptions[] = [
                    'value' => $product->getId(),
                    'label' => $product->getName()
                ];
            }
        }

        return $productOptions;
    }

    private function getProducts(){
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);
        $products = $this->productRepository->getList($this->searchCriteria);
        $productItems = $products->getItems();

        return $productItems;
    }
}