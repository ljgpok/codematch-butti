<?php

namespace Survey\SurveyPage\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Survey\SurveyPage\Model\AnswerFactory;

class Result extends Action
{
    protected $_resultPageFactory;
    /**
     * @var AnswerFactory
     */
    private $answerFactory;

    /**
     * Result constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param MessageManager $messageManager
     * @param AnswerFactory $answerFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        MessageManager $messageManager,
        AnswerFactory $answerFactory
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);

        $this->messageManager = $messageManager;
        $this->answerFactory = $answerFactory;
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();

        $params = $this->getRequest()->getParams();

        try{
            if(empty($params['name'])){
                throw new \Exception(__('Name cannot be empty'));
            }

            if(empty($params['product'])){
                throw new \Exception(__('Please select a product'));
            }

            if(empty($params['rating'])){
                throw new \Exception(__('Please provide a rank for selected product'));
            }

            if(empty($params['message'])){
                throw new \Exception(__('Please provide a message'));
            }

            $this->answerFactory->create()
                ->setName($params['name'])
                ->setRating($params['rating'])
                ->setMessage($params['message'])
                ->setProductId($params['product'])
                ->save();
        }catch(\Exception $e){
            $this->messageManager->addErrorMessage($e->getMessage());

            $redirect = $this->resultRedirectFactory->create();
            return $redirect->setPath('survey/index/index');
        }

        $this->messageManager->addSuccessMessage('Your rating was saved');

        return $resultPage;
    }
}